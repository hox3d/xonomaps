# Xonotic Maps

*Finally, he tried to.*

After setting up NetRadiant (that wasn't exactly a pleasure) you may find here my personnal projects about Xonotic mapping.

## Pillar
**The map is in a current state of non-active development.**
Intended to be large scaled, for 4 players minimum. A large, dreamy white city floating in space.
Inspired by a *Thief: Gold* level: **Mages Towers**.

## Baxement
*Okay well, the name is still WIP.*
Based on *Nexuiz Classic* **Basement** map (which was, despite an interesting layout, way too small).
So it's my take on this map, which was of course, extended greatly and rescaled to make it compatible with more than one player and a half.